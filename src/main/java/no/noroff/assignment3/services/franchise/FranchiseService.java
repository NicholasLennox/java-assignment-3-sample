package no.noroff.assignment3.services.franchise;

import no.noroff.assignment3.models.entities.MovieCharacter;
import no.noroff.assignment3.models.entities.Franchise;
import no.noroff.assignment3.models.entities.Movie;
import no.noroff.assignment3.services.CrudService;

import java.util.Set;

public interface FranchiseService extends CrudService<Franchise, Integer> {
    /**
     * Updates a list of movies for a franchise
     *
     * @param franchiseId franchise ID whose movies should be updated
     * @param movieIds movies that should replace the old set of movies
     */
    void updateMovies(int franchiseId, int[] movieIds);
    /**
     * Finds all movies for a franchise
     *
     * @param franchiseId franchise ID whose movies should be found
     * @return set of Movie objects
     */
    Set<Movie> findAllMovies(int franchiseId);

    /**
     * Finds all characters for a franchise
     *
     * @param franchiseId
     * @return set of characters
     */
    Set<MovieCharacter> findAllCharacters(int franchiseId);
}
