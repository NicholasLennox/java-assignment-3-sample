package no.noroff.assignment3.services.character;

import no.noroff.assignment3.models.entities.MovieCharacter;
import no.noroff.assignment3.services.CrudService;

public interface CharacterService extends CrudService<MovieCharacter, Integer> {
}
