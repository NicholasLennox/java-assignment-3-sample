package no.noroff.assignment3.services.character;

import no.noroff.assignment3.models.entities.MovieCharacter;
import no.noroff.assignment3.utils.exceptions.CharacterNotFoundException;
import no.noroff.assignment3.repositories.CharacterRepository;
import no.noroff.assignment3.repositories.MovieRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Set;

@Service
public class CharacterServiceImpl implements CharacterService {
    private final CharacterRepository characterRepository;
    private final MovieRepository movieRepository;

    public CharacterServiceImpl(CharacterRepository characterRepository, MovieRepository movieRepository) {
        this.characterRepository = characterRepository;
        this.movieRepository = movieRepository;
    }
    @Override
    public MovieCharacter findById(Integer id) {
        return characterRepository.findById(id).orElseThrow(() -> new CharacterNotFoundException(id));
    }

    @Override
    public Collection<MovieCharacter> findAll() {
        return characterRepository.findAll();
    }

    @Override
    public MovieCharacter add(MovieCharacter movieCharacter) {
        return characterRepository.save(movieCharacter);
    }

    @Override
    public void update(MovieCharacter movieCharacter) {
        characterRepository.save(movieCharacter);
    }

    @Override
    public void deleteById(Integer id) {
        deleteCharacterReference(id);
        characterRepository.deleteById(id);
    }

    /**
     * Deletes character references from all character's Movies in case of MovieCharacter deletion.
     *
     * @param characterId character ID that is being deleted
     */
    private void deleteCharacterReference(int characterId) {
        MovieCharacter movieCharacter = characterRepository.findById(characterId).orElseThrow(() -> new CharacterNotFoundException(characterId));
        movieCharacter.getMovies().forEach(movie -> {
            Set<MovieCharacter> chars = movie.getCharacters();
            chars.remove(movieCharacter);
            movie.setCharacters(chars);
            movieRepository.save(movie);
        });
    }
}
