package no.noroff.assignment3.services.movie;

import no.noroff.assignment3.models.entities.MovieCharacter;
import no.noroff.assignment3.models.entities.Movie;
import no.noroff.assignment3.services.CrudService;

import java.util.Set;

public interface MovieService extends CrudService<Movie, Integer> {
    /**
     * Updates the set of characters that appear in a given movie.
     *
     * @param movieId movie id whose movieCharacters should be updated
     * @param characterIds a set of MovieCharacter objects that will replace the old reference
     */
    public void updateCharacters(int movieId, int[] characterIds);
    /**
     * Finds all characters that appear in a given movie
     *
     * @param movieId movie id whose characters should be found
     * @return a set of MovieCharacter objects
     */
    public Set<MovieCharacter> findAllCharacters(int movieId);
}
