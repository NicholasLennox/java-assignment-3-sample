package no.noroff.assignment3.utils.enums;

public enum Gender {
    FEMALE,
    MALE,
    OTHER
}
