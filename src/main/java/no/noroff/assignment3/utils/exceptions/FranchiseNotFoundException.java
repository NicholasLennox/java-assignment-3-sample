package no.noroff.assignment3.utils.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value= HttpStatus.NOT_FOUND)
public class FranchiseNotFoundException extends RuntimeException {
    /**
     * Thrown if a franchise with given ID does not exist
     *
     * @param franchiseId franchise ID
     */
    public FranchiseNotFoundException(int franchiseId) {
        super("Franchise with ID " + franchiseId + " does not exist");
    }
}
