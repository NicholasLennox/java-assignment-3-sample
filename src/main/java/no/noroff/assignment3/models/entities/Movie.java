package no.noroff.assignment3.models.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "movie_id")
    private int id;
    @Column(nullable = false)
    private String title;
    @Column(length = 512)
    private String genre;
    private int releaseYear;
    private String director;
    private String picture;
    private String trailer;

    @ManyToMany
    @JoinTable(
            name = "character_movies",
            joinColumns = { @JoinColumn(name = "movie_id") },
            inverseJoinColumns = { @JoinColumn(name = "character_id") }
    )
    private Set<MovieCharacter> characters;
    @ManyToOne
    @JoinColumn(name="franchise_id")
    private Franchise franchise;
}