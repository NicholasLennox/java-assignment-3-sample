package no.noroff.assignment3.models.entities;

import lombok.*;
import no.noroff.assignment3.utils.enums.Gender;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
@Table(name = "character")
public class MovieCharacter {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "character_id")
    private int id;
    @Column(nullable = false)
    private String fullName;
    private String alias;
    @Column(length = 8)
    @Enumerated(EnumType.STRING)
    private Gender gender;
    private String picture;

    @ManyToMany(mappedBy = "characters")
    private Set<Movie> movies;
}
