package no.noroff.assignment3.models.dtos.franchise;

import lombok.Data;

@Data
public class FranchisePostDTO {
    private String name;
    private String description;
}
