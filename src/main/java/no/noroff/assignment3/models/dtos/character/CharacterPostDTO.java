package no.noroff.assignment3.models.dtos.character;

import lombok.Data;
import no.noroff.assignment3.utils.enums.Gender;

@Data
public class CharacterPostDTO {
    private String fullName;
    private String alias;
    private Gender gender;
    private String Picture;
}
