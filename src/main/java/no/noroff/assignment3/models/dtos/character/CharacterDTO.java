package no.noroff.assignment3.models.dtos.character;

import lombok.Data;
import no.noroff.assignment3.utils.enums.Gender;

import java.util.Set;

@Data
public class CharacterDTO {
    private int id;
    private String fullName;
    private String alias;
    private Gender gender;
    private String Picture;
    private Set<Integer> movies;
}
