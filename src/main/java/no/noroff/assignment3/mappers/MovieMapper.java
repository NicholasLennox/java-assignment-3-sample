package no.noroff.assignment3.mappers;

import no.noroff.assignment3.models.dtos.movie.MovieDTO;
import no.noroff.assignment3.models.dtos.movie.MoviePostDTO;
import no.noroff.assignment3.models.dtos.movie.MovieUpdateDTO;
import no.noroff.assignment3.models.entities.MovieCharacter;
import no.noroff.assignment3.models.entities.Movie;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Mapper for movies. Notice how the mapping is fairly simple and required no services.
 * This is by design and the complexity is handled through other endpoints and services.
 */
@Mapper(componentModel = "spring")
public abstract class MovieMapper {
    // Mappings from DTO to movie.
    public abstract Movie moviePostDtoToMovie(MoviePostDTO moviePostDTO);
    public abstract Movie movieUpdateDtoToMovie(MovieUpdateDTO movieUpdateDTO);
    // Mappings from movie to DTOs
    public abstract Collection<MovieDTO> movieToMovieDto(Collection<Movie> movies);
    @Mapping(target="characters", source="characters", qualifiedByName = "charactersToIds")
    @Mapping(target="franchise", source="franchise.id")
    public abstract MovieDTO movieToMovieDto(Movie movie);

    @Named("charactersToIds")
    Set<Integer> map(Set<MovieCharacter> source) {
        if (source == null) return null;
        return source.stream().map(ch -> ch.getId()
        ).collect(Collectors.toSet());
    }
}
