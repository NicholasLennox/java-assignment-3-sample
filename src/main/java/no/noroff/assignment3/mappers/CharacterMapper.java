package no.noroff.assignment3.mappers;

import no.noroff.assignment3.models.dtos.character.CharacterDTO;
import no.noroff.assignment3.models.dtos.character.CharacterPostDTO;
import no.noroff.assignment3.models.dtos.character.CharacterUpdateDTO;
import no.noroff.assignment3.models.entities.MovieCharacter;
import no.noroff.assignment3.models.entities.Movie;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Mapper for characters. Notice how the mapping is fairly simple and required no services.
 * This is by design and the complexity is handled through other endpoints and services.
 */
@Mapper(componentModel = "spring")
public abstract class CharacterMapper {
    // Mappings from DTO to character.
    public abstract MovieCharacter characterPostDtoToCharacter(CharacterPostDTO characterDto);
    public abstract MovieCharacter characterUpdateDtoToCharacter(CharacterUpdateDTO characterUpdateDTO);
    // Mappings from character to DTO
    @Mapping(target = "movies", source="movies", qualifiedByName = "moviesToIds")
    public abstract CharacterDTO characterToCharacterDto(MovieCharacter movieCharacter);
    public abstract Collection<CharacterDTO> characterToCharacterDto(Collection<MovieCharacter> movieCharacters);


    @Named("moviesToIds")
    Set<Integer> map(Set<Movie> source) {
        if (source == null) return null;
        return source.stream().map(m -> m.getId()
        ).collect(Collectors.toSet());
    }
}
