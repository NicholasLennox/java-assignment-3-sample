package no.noroff.assignment3.mappers;

import no.noroff.assignment3.models.dtos.franchise.FranchiseDTO;
import no.noroff.assignment3.models.dtos.franchise.FranchisePostDTO;
import no.noroff.assignment3.models.dtos.franchise.FranchiseUpdateDTO;
import no.noroff.assignment3.models.entities.Franchise;
import no.noroff.assignment3.models.entities.Movie;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Mapper for franchises. Notice how the mapping is fairly simple and required no services.
 * This is by design and the complexity is handled through other endpoints and services.
 */
@Mapper(componentModel = "spring")
public abstract class FranchiseMapper {
    // Mappings from DTO to franchise
    public abstract Franchise franchisePostDtoToFranchise(FranchisePostDTO franchisePostDTO);
    public abstract Franchise franchiseUpdateDtoToFranchise(FranchiseUpdateDTO franchiseUpdateDTO);
    // Mappings from franchise to DTO
    public abstract Collection<FranchiseDTO> franchiseToFranchiseDto(Collection<Franchise> franchises);
    @Mapping(target="movies", source="movies", qualifiedByName = "moviesToMovieIds")
    public abstract FranchiseDTO franchiseToFranchiseDto(Franchise franchise);

    @Named("moviesToMovieIds")
    Set<Integer> mapMoviesToIds(Set<Movie> source) {
        if (source == null) return null;
        return source.stream().map(
                m -> m.getId()
        ).collect(Collectors.toSet());
    }
}
